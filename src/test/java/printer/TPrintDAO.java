package printer;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import printer.dao.PrinterDAO;
import printer.objects.Printer;
import printer.objects.TypeCheck;
import ru.fiscal.data.PayTipe;

public class TPrintDAO {

    private PrinterDAO printerDAO;

    private Printer printer;


    @Before
    public void init(){
        printer= new Printer();
        printerDAO= new PrinterDAO(printer);

    }



    @Test
    public void paperJamedStart(){
        printerDAO.setPeperJamed(true);
        Assert.assertEquals(IFiscalPrinter.State.ERROR,printerDAO.getPrinterState());

    }

    @Test
    public void noConnectionStart(){
        printerDAO.setConnection(false);
        Assert.assertEquals(IFiscalPrinter.State.DISCONNECTED,printerDAO.getPrinterState());
        printerDAO.setConnection(true);
    }

    @Test
    public void end24HStart(){
        printerDAO.set24H(true);
        printerDAO.setZReport(false);
        printerDAO.setDayOpen(true);

        Assert.assertEquals(IFiscalPrinter.State.Z_REPORT_EXPIRED,printerDAO.getPrinterState());

    }

    @Test
    public void paperOnStart(){

        printerDAO.setNoPaper(true);
        Assert.assertEquals(IFiscalPrinter.State.NOT_PAPER,printerDAO.getPrinterState());
        printerDAO.setNoPaper(false);

    }

    @Test
    public void end24HZreportStart(){

        printerDAO.setDayOpen(false);
        printerDAO.set24H(false);
        printerDAO.setZReport(true);
        Assert.assertEquals(IFiscalPrinter.State.IDLE,printerDAO.getPrinterState());

    }

    @Test
    public void openReceiptStart(){
        printerDAO.setReceiptIsOpen(true);
        printerDAO.setDayOpen(true);
        Assert.assertEquals(IFiscalPrinter.State.RECEIPT_IS_OPEN,printerDAO.getPrinterState());
        printerDAO.setReceiptIsOpen(false);

    }

    @Test
    public void isDayOpen(){
        printerDAO.setDayOpen(true);
        Assert.assertEquals(IFiscalPrinter.State.READY,printerDAO.getPrinterState());
        printerDAO.setDayOpen(false);
    }

    @Test
    public void openDay(){
        printer= new Printer();
        printerDAO= new PrinterDAO(printer);
        try {
            printerDAO.openDay();
        } catch (Exception e) {}
        Assert.assertEquals(IFiscalPrinter.State.READY,printerDAO.getPrinterState());
    }

    @Test
    public void openReceipt(){
        printer= new Printer();
        printerDAO= new PrinterDAO(printer);
        try {
            printerDAO.openDay();
            printerDAO.openReceipt();
        } catch (Exception e) {}
        Assert.assertEquals(IFiscalPrinter.State.RECEIPT_IS_OPEN,printerDAO.getPrinterState());
    }

    @Test
    public void closeReceipt(){
        printer= new Printer();
        printerDAO= new PrinterDAO(printer);
        try {
            printerDAO.openDay();
            printerDAO.openReceipt();
            printerDAO.printAndCloseReceipt(TypeCheck.TRANSACTION, PayTipe.CASH,1000,"");
        } catch (Exception e) {}
        Assert.assertEquals(IFiscalPrinter.State.READY,printerDAO.getPrinterState());
    }

    @Test
    public void cancelReceipt(){
        printer= new Printer();
        printerDAO= new PrinterDAO(printer);
        try {
            printerDAO.openDay();
            printerDAO.openReceipt();
            printerDAO.cancelReceipt();
        } catch (Exception e) {}
        Assert.assertEquals(IFiscalPrinter.State.READY,printerDAO.getPrinterState());
    }

    @Test
    public void closeDay(){
        printer= new Printer();
        printerDAO= new PrinterDAO(printer);
        try {
            printerDAO.openDay();
            printerDAO.closeDay();
            Assert.assertEquals(IFiscalPrinter.State.Z_REPORT_EXPIRED,printerDAO.getPrinterState());
        } catch (Exception e) {}

    }

    @Test
    public void printZrep(){
        printer= new Printer();
        printerDAO= new PrinterDAO(printer);
        try {
            printerDAO.openDay();
            printerDAO.closeDay();
            printerDAO.printZReport();
            Assert.assertEquals(IFiscalPrinter.State.IDLE,printerDAO.getPrinterState());
        } catch (Exception e) {}

    }

    @Test
    public void virtuaPrintZrep(){
        printer= new Printer();
        printerDAO= new PrinterDAO(printer);
        try {
            printerDAO.openDay();
            printerDAO.closeDay();
            printerDAO.printOff();
            printerDAO.printZReport();
            Assert.assertEquals(IFiscalPrinter.State.IDLE,printerDAO.getPrinterState());
        } catch (Exception e) {}

    }

    @Test
    public void virtuaPrintZrepAndError(){
        printer= new Printer();
        printerDAO= new PrinterDAO(printer);
        try {
            printerDAO.openDay();
            printerDAO.setNoPaper(true);
            Assert.assertEquals(IFiscalPrinter.State.NOT_PAPER,printerDAO.getPrinterState());
            printerDAO.closeDay();
            printerDAO.printOff();
            printerDAO.printZReport();
            printerDAO.setNoPaper(false);

            Assert.assertEquals(IFiscalPrinter.State.IDLE,printerDAO.getPrinterState());
        } catch (Exception e) {}

    }
}
