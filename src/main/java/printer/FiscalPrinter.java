package printer;

import org.apache.log4j.Logger;
import printer.dao.Despether;
import printer.dao.PrinterDAO;
import printer.objects.*;
import ru.fiscal.data.PayTipe;


import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;


public class FiscalPrinter implements IFiscalPrinter {

    private PrinterDAO printerDAO;
    private Printer printer;

    private static Logger log = Logger.getLogger(FiscalPrinter.class);

    PrintWriter printWriter;



    public FiscalPrinter() {
        printer = new Printer();
        printerDAO = new PrinterDAO(printer);


        //log.debug(printer);

       /*  try {
           boolean running= true;
           ServerSocket ss =  new ServerSocket(9999);
           Despether despether= new Despether(printer);
           new Thread(()->{





                try  (Socket socket = ss.accept();
                    BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
                    PrintWriter printWriter= new PrintWriter(bw);) {

                this.printWriter=printWriter;
                despether.setWriter(printWriter);

                    //System.out.println((char) is.read());
                while(socket.isConnected()){
                 //while( br.lines().count()==0){Thread.sleep(10);}

                 String value=br.readLine();
                 if(value == null)
                     break;

                 log.info(value);

                despether.meneger(value);*/

               /* switch (value.toString()) {

                    case "19":
                        openReceipt();
                        continue;
                    case "20":
                        sale("testProduct", 300, 4);
                        continue;
                    case "21":
                        sale("testp2", 600);
                        continue;
                    case "22":
                        closeReceipt("чек закрыт", 10000, 0, 0, 0, 0);
                        continue;
                    case "23":
                        cancelReceipt();
                        continue;
                    case "24":
                        cashOut(100);
                        continue;
                    case "25":
                        cashIn(300);
                        continue;
                    case "26":
                        printZReport();
                        continue;
                    case "27":
                        bufferZReport();
                        continue;
                    case "28":
                        log.debug(getPrinterState());
                        continue;
                    case "29":
                        log.debug(getPrinterState());
                        continue;

                }

           }



        }
                catch (Exception e) {
                    e.printStackTrace();
                }

        }).start();
    } catch (IOException ee) {
        ee.printStackTrace();
    }*/
    }



    @Override
    public synchronized void openReceipt() throws Exception {

        try {
            if (getPrinterState()==State.IDLE){
                printerDAO.openDay();
      //          printWriter.write("вызван openReceipt, открытие смены");
            }


            printerDAO.openReceipt();
            log.debug("чек открыт");
    //       printWriter.write("чек открыт");
        } catch (Exception e) {
            log.error("ошибка открытия чека",e);
     //     printWriter.write("ошибка открытия чека: "+e.getMessage());

        //    throw e;

        }finally {
//            printWriter.write("\n");
//            printWriter.flush();
        }


    }

    @Override
    public synchronized void sale(String name, long price) throws Exception {
            sale(name, price, 1);
    }

    @Override
    public synchronized void sale(String name, long price, long quantity) throws Exception {
        try {
            printerDAO.sale(name, price/100, quantity);
            log.debug("продукт добавлен " + name + " цена:" + price + " колличество:" + quantity);
        } catch (Exception e) {
            log.error("ошибка добавление продукта");
          //  throw e;
        }
    }

    @Override
    public synchronized void cancelReceipt() throws Exception {

        try {
           String ch= printerDAO.cancelReceipt();
            log.debug("чек отменен\n"+ch);
        } catch (Exception e) {
            log.error("ошибка закрытия чека",e);
           // throw e;
        }
    }

    @Override
    public synchronized long closeReceipt(String text, long payTypeAmount1,
                             long payTypeAmount2,
                             long payTypeAmount3,
                             long payTypeAmount4, int discount) throws Exception {

        try{
        String stringCheck;
        printer.setNds(Integer.toString(discount));
        if (payTypeAmount1 != 0) {
            stringCheck =
                    printerDAO.printAndCloseReceipt(TypeCheck.TRANSACTION, PayTipe.CASH, payTypeAmount1, text);

        } else if (payTypeAmount2 != 0) {

            stringCheck =
                    printerDAO.printAndCloseReceipt(TypeCheck.TRANSACTION, PayTipe.ELECTRONIC, payTypeAmount1, text);
        } else if (payTypeAmount3 != 0) {
            stringCheck =
                    printerDAO.printAndCloseReceipt(TypeCheck.TRANSACTION, PayTipe.CASH, payTypeAmount1, text);
        } else if (payTypeAmount4 != 0) {
            stringCheck =
                    printerDAO.printAndCloseReceipt(TypeCheck.TRANSACTION, PayTipe.CASH, payTypeAmount1, text);
        } else
            stringCheck =
                    printerDAO.printAndCloseReceipt(TypeCheck.TRANSACTION, PayTipe.CASH, payTypeAmount1, text);

        log.info("receipt closed:"+"\n"+stringCheck);
    }catch (Exception e){
            log.error("ошибка оплаты товаров",e);
         //   throw e;
        }
        return 0;
    }

    @Override
    public synchronized void bufferZReport() throws Exception {
        try {
            printerDAO.printOff();
            printZReport();
            printerDAO.printOn();
        }
       catch (Exception e) {
         log.error("ошибка вертуальной печати z-report",e);
      //   throw e;
        }
    }

    @Override
    public synchronized void printBufferedZReport() throws Exception {
       bufferZReport();
    }

    @Override
    public synchronized void printZReport() throws Exception {
        try {

                printerDAO.closeDay();
           String ch= printerDAO.printZReport();
           log.debug("печать z-report\n"+ch);

        }catch (Exception e){
            log.error("ошибка печати z-report",e);
        }
    }

    @Override
    public void schedulePrintZReport(Date when, long periodInMin) { }

    @Override
    public void scheduleBufferZReport(Date when, long periodInMin) { }

    @Override
    public void feedPaper(int length) throws Exception {
        log.debug("paper is feed");
    }

    @Override
    public void cutPaper() throws Exception {
         log.debug("paper is cut");
    }

    @Override
    public void printString(String str) throws Exception {

    }

    @Override
    public synchronized State getPrinterState() throws Exception {
        return printerDAO.getPrinterState();
    }

    @Override
    public void printDuplicateReceipt() throws Exception {

    }

    @Override
    public synchronized void cashIn(long amount) throws Exception {
        try {
            log.debug(printerDAO.putSum(Long.toString(amount/100)));
    }catch (Exception e){
          log.error("нельзя ложить деньги при открытом чеке",e);
         // throw  e;
    }
    }

    @Override
    public synchronized void cashOut(long amount) throws Exception {
        try {
            log.debug(printerDAO.takeSum(Long.toString(amount / 100)));

        }catch (Exception e){
            log.error("нельзя доставать деньги при открытом чеке",e);
        //    throw e;
        }
    }

    @Override
    public synchronized void printXReport() throws Exception {
           printZReport();
    }

    public Printer getPrinter() {
        return printer;
    }
}
