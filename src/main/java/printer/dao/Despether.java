package printer.dao;

import printer.objects.Printer;

import java.io.BufferedWriter;
import java.io.PrintWriter;
import java.util.Objects;

public class Despether {


    private  Printer printer;

    PrintWriter pw;
    public Despether(Printer printer){
        this.printer=printer;
    }


    public void setWriter(PrintWriter pw){
        this.pw=pw;
    }

    public void meneger(String str) {
        if (Objects.isNull(str))
            return;

        if (str.equals("1")) {
            printer.setDayIsOpen(true);
            write(String.format("флаг openDay - %b",true));

        } else if (str.equals("2")) {
            printer.setDayIsOpen(false);
            write(String.format("флаг openDay - %b",false));

        } else if (str.equals("3")) {
            printer.setReceiptIsOpen(true);
            write(String.format("флаг openReceipt - %b",true));

        } else if (str.equals("4")) {
            printer.setReceiptIsOpen(false);
            write(String.format("флаг openReceipt - %b",false));

        } else if (str.equals("5")){
            printer.setDay24H(true);
            write(String.format("флаг day25H - %b",true));


         }else if (str.equals("6")) {
            printer.setDay24H(false);
            write(String.format("флаг day25H - %b",false));

        } else if (str.equals("7")) {
            printer.setNoPaper(true);
            write(String.format("флаг noPaper - %b",true));

        } else if (str.equals("8")) {
            printer.setNoPaper(false);
            write(String.format("флаг noPaper - %b",false));

        } else if (str.equals("9")) {
            printer.setPiperJamed(true);
            write(String.format("флаг PiperJamed - %b",true));

        } else if (str.equals("10")){
            printer.setPiperJamed(false);
        write(String.format("флаг PiperJamed - %b",false));


        }else if (str.equals("11")){
            printer.setDorOpen(true);
        write(String.format("флаг DorOpen - %b",true));

        }else if(str.equals("12")) {
            printer.setDorOpen(false);
            write(String.format("флаг DorOpen - %b",false));

        } else if(str.equals("13")) {
            printer.setPrintHeadError(true);
            write(String.format("флаг PrintHeadError - %b",true));

        }else if(str.equals("14")) {
            printer.setPrintHeadError(false);
            write(String.format("флаг PrintHeadError - %b",false));

        } else if(str.equals("15")) {
            printer.setzReport(true);
            write(String.format("флаг zReport - %b",true));

        }else if(str.equals("16")) {
            printer.setzReport(false);
            write(String.format("флаг zReport - %b",false));

        }else if(str.equals("17")) {
            printer.setConnected(false);
            write(String.format("флаг desConnected - %b",true));

        }else if(str.equals("18")) {
            printer.setConnected(true);
            write(String.format("флаг desConnected - %b",false));

        } else if(str.equals("19")) {
            printer.setPrintOn(true);
            write(String.format("флаг PrintOn - %b",true));

        } else if(str.equals("20")) {
            printer.setPrintOn(false);
            write(String.format("флаг PrintOn - %b",false));

        }else if(str.equals("21")) {
            printer.setLowPaper(true);
            write(String.format("флаг LowPaper - %b",true));

        }else if(str.equals("22")) {
            printer.setLowPaper(false);
            write(String.format("флаг LowPaper - %b",false));
        }


    }

    private void write(String data){
        pw.write(data+"\n");
        pw.flush();
    }
}
