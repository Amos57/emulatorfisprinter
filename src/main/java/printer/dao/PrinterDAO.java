package printer.dao;

import printer.objects.*;
import ru.fiscal.data.PayTipe;
import static printer.IFiscalPrinter.State;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import java.util.*;

public class PrinterDAO {

    private Printer printer;

     @Unused
    private ArrayList<Product> productes;

     @Unused
    private NumberFormat formatNumber = new DecimalFormat("#0.00");
     @Unused
    private NumberFormat formatNumberCount = new DecimalFormat("0000");


    public PrinterDAO(Printer printer) {
        this.printer = printer;
        monitorState();
        productes= new ArrayList<>();
    }


    /**
     *
     * добавление суммы в кассу
     * используется во время инкасации или оплаты товара
     * @param sum
     */
    private void putSum(BigDecimal sum){
         printer.setSum(printer.getSum().add(sum));
         printer.setAllSumInBox(printer.getAllSumInBox().add(sum));
    }


    /**
     *
     * положить сумму в кассу
     * во время инкасации
     *
     * @param sum
     */
    public String putSum(String sum) throws Exception {
        checkConnected();
        isError();
        if(Objects.isNull(sum) || (  printer.getState()!=State.IDLE && printer.getState()!=State.READY))
           throw new Exception();

        putSum(new BigDecimal(sum));
        return String.format("положенно в кассу %s деньжат",sum);
    }


    /**
     *
     * изъятие суммы во время инкасации
     *
     * @param sum
     * @return
     */
    public String takeSum(String sum) throws Exception {
        checkConnected();
        isError();
        if(Objects.isNull(sum) || ( printer.getState()!=State.IDLE && printer.getState()!=State.READY))
            throw new Exception();


     /*   BigDecimal s = new BigDecimal(sum).abs();

        //сравниваем целочисленные части
     *//*   if(printer.getSum().intValue()<s.intValue()){
            //TODO write log
            return null;
        }*//*

        printer.setSum(printer.getSum().subtract(s));
        printer.setAllSumInBox(printer.getAllSumInBox().subtract(s));*/

        return String.format("взято из кассы %s деньжат",sum);
    }




    public String cancelReceipt() throws Exception{
        return printAndCloseReceipt(TypeCheck.CANCEL_RECEIPT,null,0, null);
    }

    /**
     *
     *
     *
     *
     * @param
     * @param typeCheck тип распечатамого чека, для данного метода параметро 2:
     *                 </p> 1)CANCEL_RECEIPT
     *                 </p> 2)TRANSACTION
     * @param text
     * @return
     */
    public String printAndCloseReceipt(TypeCheck typeCheck, PayTipe pt, long many, String text) throws Exception {

        checkConnected();
          isError();
        //проверка состояния принтера
        if (printer.getState()!=State.RECEIPT_IS_OPEN){

            throw new Exception("не допустимо в данном статусе");
        }

       // check.setProductes(productes);
        printer.setDate(new Date(System.currentTimeMillis()));

        Check check= new Check();
        check.setTypePay(pt);
        check.setSum(new BigDecimal(many));
        check.setText(text);
        /**
         * начнем собирать чек
         */
        StringBuilder sb= new StringBuilder("ИНН:"+printer.getInn()+"\n\n");
        sb.append("МЕСТО РАСЧЕТОВ:\n");
        sb.append("\tКАССОВЫЙ ЧЕК");
        sb.append(printer.getHeader()+"\n");
        sb.append("\tПРИХОД\n");
        BigDecimal sumOftheItem = BigDecimal.ZERO;//общая сумма за все товары

        for(Product product : productes){
            sb.append(check.getText()+"\n");



            //суммируем к общей оплате
            sumOftheItem=sumOftheItem.add(new BigDecimal(product.getPrice()));

            sb.append(product.getName()+" ");
            sb.append(product.getQuatity()+"*"+product.getPrice());
            sb.append("\t="+formatNumber.format(product.getPrice())+"\n");
            sb.append("НДС "+printer.getNds()+"%\n");
        }

        check.setSumOftheItem(sumOftheItem);//сохраняем информацию о сумме за продукты

        if(typeCheck.equals(TypeCheck.CANCEL_RECEIPT)){
            sb.append("\t***ЧЕК ОТМЕНЕН***");

            //printer.getCencelChecks().add(check); // сохраняем информацию об отмененном чеке
        }else if(typeCheck.equals(TypeCheck.TRANSACTION)){

            if(check.getSum().compareTo(sumOftheItem)<0){
              /**
                 *  кладем чек с ошибкой в отменненные чеки,
                 *  поскольку в нем ошибочные данные о внесенной сумме
                  */
                printer.getCencelChecks().add(check);
                throw new Exception("сумма маленькая для оплаты");
            }


            sb.append("ИТОГ\t=" + formatNumber.format(sumOftheItem)+"\n");
            sb.append("СУММА С НДС"+printer.getNds()+"%"+"\t="+formatNumber.format(sumOftheItem)+"\n");
            sb.append(check.getTypePay().getDescription()+"\t="+formatNumber.format(check.getSum()));



            if(!check.getSum().equals(sumOftheItem)){
                sumOftheItem=check.getSum().subtract(sumOftheItem);
                sb.append("\nСДАЧА"+"\t="+formatNumber.format(sumOftheItem));
            }
            putSum(sumOftheItem);

            printer.getAllChecks().add(check);

        }

        printer.setReceiptIsOpen(false);
        mutableState();
        productes.clear();
            return sb.toString();
    }

    /**
     * @return true if success,false if fail
     */
    public boolean openReceipt() throws Exception{
        checkConnected();
        isError();

        if(printer.getState()!=State.READY){

            throw new Exception("не выполнимо в данном статусе");
         };

        printer.setReceiptIsOpen(true);

        mutableState();
        return printer.isReceiptIsOpen();
    }



    public boolean closeDay() throws Exception {
        if(printer.getState()!=State.READY &&
                printer.getState()!=State.ERROR ||
                printer.isReceiptIsOpen())
            throw new Exception("нельзя закрыть смену в этом стутусе");

        if((printer.getState()==State.ERROR && printer.isPrintOn()) )
            throw new Exception("нельзя закрыть смену в этом стутусе");

        printer.setDayIsOpen(false);
        mutableState();
        return printer.isDayIsOpen();
    }

    public boolean openDay() throws Exception {
        checkConnected();

         if(printer.getState()==State.IDLE) {

             printer.setDayIsOpen(true);
             printer.setzReport(false);
             mutableState();
         } else
             throw new Exception("день не может быть открыт в данном статусе");



        return printer.isDayIsOpen();
    }


    public boolean printOff(){
        printer.setPrintOn(false);
        return printer.isPrintOn();
    }

    public boolean printOn(){
          printer.setPrintOn(true);
          return printer.isPrintOn();
    }

    public String printZReport() throws Exception {


        if(printer.getState()!=State.Z_REPORT_EXPIRED &&
                printer.getState()!=State.READY && printer.getState()!=State.ERROR)
            throw new Exception("не возможна печать z-report  при  статусе "+ printer.getState());


        if((printer.getState()==State.ERROR && printer.isPrintOn())
                ||printer.isDayIsOpen() || printer.isReceiptIsOpen())
            throw new Exception("не возможна печать z-report в статусе ERROR c включенной печатью");



                List<Check> checksSuccess = printer.getAllChecks();
               // List<Check> checksCencel = printer.getCencelChecks();


                StringBuilder sb = new StringBuilder(printer.getHeader() + "\n");


                sb.append("\tОТЧ. О ЗАКР. СМ.");
                sb.append("КАССИР:\n");

                sb.append("СУММА С НДС " + printer.getNds() + "%\n");
                sb.append("ПРИХОД\t=" + printer.getSum() + "\n");
                sb.append("ВОЗВРАТ ПРИХОДА\t" + "=?\n");
                sb.append("РАСХОД\t" + "=?\n");
                sb.append("ВОЗВРАТ РАСХОДА\t" + "=?\n");

                sb.append("СЕКЦИЯ №1");
                sb.append("0004 ПРИХОД\t=" + printer.getSum() + "\n");
                sb.append("0000 ВОЗВРАТ ПРИХОДА\t" + "=?\n");
                sb.append("0000 РАСХОД\t" + "=?\n");
                sb.append("0000 ВОЗВРАТ РАСХОДА\t" + "=?\n");

                sb.append("ИТОГОВЫЕ СУММЫ\n");
                sb.append("ПРИХОД\t=" + printer.getSum() + "\n");
                sb.append("ВОЗВРАТ ПРИХОДА\t" + "=?\n");
                sb.append("РАСХОД\t" + "=?\n");
                sb.append("ВОЗВРАТ РАСХОДА\t" + "=?\n");

                sb.append("КОРРЕКЦИЙ");
                sb.append("0004 ПРИХОД\t=?\n");
                sb.append("0000 ВОЗВРАТ ПРИХОДА\t" + "=?\n");
                sb.append("0000 РАСХОД\t" + "=?\n");
                sb.append("0000 ВОЗВРАТ РАСХОДА\t" + "=?\n");


                sb.append("АННУЛИРОВАНИЙ\t" + printer.getCountZeroize());
                sb.append("ПРИХОД\t=?\n");
                sb.append("ВОЗВРАТ ПРИХОДА\t" + "=?\n");
                sb.append("РАСХОД\t" + "=?\n");
                sb.append("ВОЗВРАТ РАСХОДА\t" + "=?\n");

                sb.append("НАКОПЛЕНИЙ НА НАЧАЛО СМЕНЫ\nПРИХОД\t=" + printer.getSum());

                sb.append("ПРИХОД\t" + formatNumberCount.format(checksSuccess.size()) + "\n");

                sb.append("ПРИХОД\t=" + printer.getCount());

                sb.append("В ДЕНЕЖНОМ ЯЩИКЕ\nНАЛИЧННЫМИ\t=" + formatNumber.format(printer.getAllSumInBox()));

                printer.setzReport(true);
                mutableState();
                return sb.toString();

    }



    private  boolean isError() throws Exception {

        if(printer.getState()==State.NOT_PAPER) {
            throw new Exception("нет бумаги");
        }
        if(printer.isPiperJamed()) {
            throw new Exception("бумага замята");
        }
        if(printer.isDorOpen()) {
            throw new Exception("открыта крышка принтера");
        } if (printer.isPrintHeadError()) {
            throw new Exception("нет бумаги");
        }
        return true;

    }

    public boolean sale(String name, long price, long quantity) throws Exception{
        checkConnected();
        isError();
        if(printer.getState()!=State.RECEIPT_IS_OPEN)
            throw new Exception("добавление продукта не выполнимо при данном статусе");

        return productes.add(new Product(name,quantity,price/100*quantity));

    }

    private void checkConnected() throws Exception {
        if(printer.getState()==State.DISCONNECTED) {
            throw new Exception("статус DISCONNECTED");
        }

    }
    public State getPrinterState(){
        return printer.getState();
    }

    public  void monitorState(){

        Thread thread= new Thread(() -> {


            while(true) {

                try {

                    mutableState();
                    Thread.sleep(10);

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        });
        thread.setDaemon(true);
        thread.start();
    }

    private synchronized void mutableState(){


        if(!printer.isConnection())
            printer.setState(State.DISCONNECTED);
        else if(printer.isNoPaper())
            printer.setState(State.NOT_PAPER);
        else if(printer.isPrintHeadError() || printer.isPiperJamed() || printer.isDorOpen())
            printer.setState(State.ERROR);
        else if(printer.isDayIsOpen() && !printer.isReceiptIsOpen() && !printer.isDay24H())
            printer.setState(State.READY);
        else if(printer.isDayIsOpen() && printer.isReceiptIsOpen() && !printer.isDay24H())
            printer.setState(State.RECEIPT_IS_OPEN);
        else if((printer.isDay24H() && !printer.iszReport()) || (!printer.isDayIsOpen()&&!printer.iszReport()))
            printer.setState(State.Z_REPORT_EXPIRED);
        else if(!printer.isDayIsOpen() && !printer.isReceiptIsOpen() && printer.iszReport())
            printer.setState(State.IDLE);

    }

    @ForTest
    public void setPeperJamed(boolean f){
        printer.setPiperJamed(f);
        mutableState();
    }


    @ForTest
    public void setNoPaper(boolean f){
        printer.setNoPaper(f);
        mutableState();
    }

    @ForTest
    public void set24H(boolean f) {
        printer.setDay24H(f);
        mutableState();
    }


    @ForTest
    public void setZReport(boolean f){
        printer.setzReport(f);
        mutableState();
    }

    @ForTest
    public void setConnection(boolean b) {
        printer.setConnected(b);
        mutableState();
    }

    @ForTest
    public void setReceiptIsOpen(boolean b){
        printer.setReceiptIsOpen(b);
        mutableState();
    }


    @ForTest
    public void setDayOpen(boolean b) {
        printer.setDayIsOpen(b);
        mutableState();
    }
}


