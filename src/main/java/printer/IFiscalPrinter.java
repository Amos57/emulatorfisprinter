package printer;



import java.util.Date;

public interface IFiscalPrinter {


    enum State {
        /**
         * <p>Indicates no communication with printer Note:
         * to work with printer you need open serial
         * port using method open() port in the class
         * SerialPrinterPort;</p>
         */
        DISCONNECTED,

        /**
         *<p>Session is not opened. Printer ready to work</p>
         */
        IDLE,

        /**
         * <p>Session is opened. Printer ready to work</p>
         */
        READY,

        /**
         * <p>Session can't be open because Z-Report not been
         * used last 24 hours after opening session</p>
         */
        Z_REPORT_EXPIRED,

        /**
         * <p>Receipt available. For the following activities
         * necessary to close a receipt using
         * closeReceipt(ReceiptData data) or to cancel
         * the receipt using cancelReceipt()</p>
         */
        RECEIPT_IS_OPEN,

        /**
         * <p>No receipt paper</p>
         */
        NOT_PAPER,

        ERROR
    }


    /**
     * <P>The method is used to open the check</P>
     * <p>The first invocation opens the session. For close use printZReport() or printBufferedZReport()</p>
     * <p>This method can't be used if receipt is opened</p>
     *
     * @throws Exception not description
     */
    void openReceipt() throws Exception;

    /**
     * @param name     product name (parking time)
     * @param price    price 1 value == 0.01 real unit
     * @throws Exception not description
     */
    void sale(String name, long price) throws Exception;

    /**
     * @param name     product name (parking time)
     * @param price    price 1 value == 0.01 real unit
     * @param quantity quantity 1 value == 0.001 real unit
     * @throws Exception not description
     */
    void sale(String name, long price, long quantity) throws Exception;

    /**
     * <p>Use if a receipt cannot be closed</p>
     *
     * @throws Exception not description
     */
    void cancelReceipt() throws Exception;

    /**
     * @param text           finally text
     * @param payTypeAmount1 paying amount type cash (1 value == 0.001 real unit)
     * @param payTypeAmount2 paying amount type credit card (1 value == 0.001 real unit)
     * @param payTypeAmount3 paying amount type package (1 value == 0.001 real unit)
     * @param payTypeAmount4 paying amount transport card (1 value == 0.001 real unit)
     * @param discount       discount in percentage (1 value == 0.01%)
     * @return amount change in (1 value == 0.001 real unit)
     * @throws Exception not description
     */
    long closeReceipt(String text, long payTypeAmount1, long payTypeAmount2,
                      long payTypeAmount3, long payTypeAmount4, int discount) throws Exception;


    /**
     * <p>Used for closing a session and store report in memory</p>
     *
     * @throws Exception not description
     */
    void bufferZReport() throws Exception ;

    /**
     * <p>Print all reports from memory</p>
     *
     * @throws Exception not description
     */
    void printBufferedZReport() throws Exception;

    /**
     * <p>Closing session and print report</p>
     *
     * @throws Exception not description
     */
    void printZReport() throws Exception;

    /**
     * @param when        when will the first printZReport()
     * @param periodInMin period printing reports (minutes)
     */
    void schedulePrintZReport(Date when, long periodInMin);

    /**
     * @param when        when will the first printBufferedZReport()
     * @param periodInMin period buffering reports (minutes)
     */
    void scheduleBufferZReport(Date when, long periodInMin);

    /**
     * <p>Print empty lines</p>
     *
     * @param length amount lines
     * @throws Exception not descriptions
     */
    void feedPaper(int length) throws Exception;

    void cutPaper() throws Exception ;

    void printString(String str) throws Exception;

    /**
     * <p>This method provides the current printer status</p>
     * <p>
     * State.DISCONNECTED - indicates no communication with printer
     * Note: to work with printer you need open serial port using method open()
     * port in the class SerialPrinterPort;
     * <p>
     * State.IDLE - session is not opened. Printer ready to work
     * <p>
     * State.READY - session is opened. Printer ready to work
     * <p>
     * State.Z_REPORT_EXPIRED - session can't be open because Z-Report not been
     * used last 24 hours after opening session
     * <p>
     * State.RECEIPT_IS_OPEN - receipt available. For the following activities necessary to close a receipt
     * using closeReceipt(ReceiptData data) or to cancel the receipt
     * using cancelReceipt()
     * <p>
     * State.NOT_PAPER - no receipt papper
     * <p>
     * State.NOT_MEMORY - not enough memory to store Z-Report
     * <p>
     *
     * @return State
     * @throws Exception not description
     */
    public State getPrinterState() throws Exception;

    /**
     * <p>Print duplicate the last receipt</p>
     *
     * @throws Exception not description
     */
    void printDuplicateReceipt() throws Exception ;

    /**
     * <p>Insert cash in fiscal printer<p/>
     * @param amount amount cash
     * @throws Exception
     */
    void cashIn(long amount) throws Exception;

    /**
     * <p>Take cash from fiscal printer</p>
     * @param amount amount cash
     * @throws Exception
     */
    void cashOut(long amount) throws Exception;

    void printXReport() throws Exception;

}

