package printer.objects;

import org.apache.log4j.Logger;
import printer.FiscalPrinter;

import java.io.BufferedReader;
import java.io.InputStreamReader;


public class Main {

    private static Logger log = Logger.getLogger(Main.class);

    public static void main(String...args) throws Exception {


       FiscalPrinter fp= new FiscalPrinter();
        log.debug(fp.getPrinterState());
   BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
 boolean ruunning = true;
 String value;
       while(ruunning){
           value=br.readLine();

           switch (value){

               case "1":
                   fp.openReceipt();
                   log.debug(fp.getPrinterState());
                   continue;
               case "2":
                   fp.sale("testProduct",300,4);
                   continue;
               case "3":
                   fp.sale("testp2",600);
                   continue;
               case "4":
                   fp.closeReceipt("чек закрыт",10000,0,0,0,0);
                   log.debug(fp.getPrinterState());
                   continue;
               case "5":
                   fp.cancelReceipt();
                   log.debug(fp.getPrinterState());
                   continue;
               case "6":
                   fp.cashOut(100);
                   continue;
               case"7":
                   fp.cashIn(300);
                   continue;
               case"8":
                   fp.printZReport();
                   log.debug(fp.getPrinterState());
                   continue;
               case "9":
                   fp.bufferZReport();
                   log.debug(fp.getPrinterState());
                   continue;
               case"10":
                   log.debug(fp.getPrinterState());
                   continue;
               case"11":
                   fp.getPrinter().setPiperJamed(true);
                   log.debug("бумага замята");
                   continue;
               case"12":
                   fp.getPrinter().setPiperJamed(false);
                   log.debug("замятие устранено");
                   continue;
               case"13":
                   fp.getPrinter().setConnected(false);
                   log.debug("ошибка в подключении");
                   continue;
               case"14":
                   fp.getPrinter().setConnected(true);
                   log.debug("подключение восстановленно");
                   continue;
               case"0":
                   ruunning=false;

           }
       }
    }
}
