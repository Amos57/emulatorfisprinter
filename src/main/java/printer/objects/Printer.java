package printer.objects;

import static printer.IFiscalPrinter.State;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Printer implements Serializable {


    private String inn;
    private String znKKT;
    private String rhKKT;
    private String fh;


    /**
     *
     * НДС компании
     *
     * @default value "0"
     */
    private String nds="0";


    /**
     *
     * счетчик приходов денежных средств
     *
     * @Unused
     */
    @Unused
    private int count;


    /**
     *
     * счетчик отмененных открытых чеков
     *
     */
    @Unused
    private int countZeroize;


    /**
     *
     * текущая дата
     * и время в принтере
     */
    private Date date;



    private  State state = State.IDLE;

    //flags
    private boolean printOn=true;
    private boolean lowPaper;
    private boolean noPaper;
    private boolean printHeadError;
    private boolean piperJamed;
    private boolean dorOpen;
    //end flags

    private boolean isConnected=true;
    /**
     *
     * Был  ли отчет или нет
     */
    private boolean zReport=true; 

    //state
    private boolean dayIsOpen;
    private boolean receiptIsOpen;
    private boolean day24H;
    //end state

    @Unused
    private ArrayList<Check> successChecks;
    @Unused
    private ArrayList<Check> cencelChecks;

    //fool sum in cass
    @Unused
    private BigDecimal allSumInBox;


    //fool sum in cass
    @Unused
    private BigDecimal sum;




    public Printer(){
       sum=BigDecimal.ZERO;
       allSumInBox=BigDecimal.ZERO;
        successChecks= new ArrayList<>();
        cencelChecks= new ArrayList<>();

    }

    public boolean iszReport(){
        return zReport;
    }

    public  void setzReport(boolean zReport){
        this.zReport=zReport;
    }

    @Unused
    public int incrCount(){
        return count++;
    }

    @Unused
    public int getCount(){
        return count;
    }

    @Unused
    public void zeroize(){
        count=0;
        sum=BigDecimal.ZERO;
    }

    public String getInn() {
        return inn;
    }

    public void setInn(String inn) {
        this.inn = inn;
    }

    public String getZnKKT() {
        return znKKT;
    }

    public void setZnKKT(String znKKT) {
        this.znKKT = znKKT;
    }

    public String getRhKKT() {
        return rhKKT;
    }

    public void setRhKKT(String rhKKT) {
        this.rhKKT = rhKKT;
    }

    public String getFh() {
        return fh;
    }

    public void setFh(String fh) {
        this.fh = fh;
    }

    public boolean isDay24H() {
        return day24H;
    }

    public void setDay24H(boolean day24H) {
        this.day24H = day24H;
    }


    @Unused
    public BigDecimal getAllSumInBox() {
        return allSumInBox;
    }

    @Unused
    public void setAllSumInBox(BigDecimal allSumInBox) {
        this.allSumInBox = allSumInBox;
    }




    @Unused
    public BigDecimal getSum() {
        return sum;
    }

    @Unused
    public void setSum(BigDecimal sum) {
        this.sum = sum;
    }

    @Unused
    public ArrayList<Check> getCencelChecks() {
        return cencelChecks;
    }

   // public void setCencelChecks(ArrayList<Check> cencelChecks) {
 //   this.cencelChecks = cencelChecks;
   // }

     @Unused
    public ArrayList<Check> getAllChecks() {
        return successChecks;
    }
    @Unused
    public void setAllChecks(ArrayList<Check> successChecks) {
        this.successChecks = successChecks;
    }

    public boolean isPrintOn() {
        return printOn;
    }

    public void setPrintOn(boolean printOn) {
        this.printOn = printOn;
    }

    public  boolean isLowPaper() {
        return lowPaper;
    }

    public void setLowPaper(boolean lowPaper) {
        this.lowPaper = lowPaper;
    }

    public  boolean isNoPaper() {
        return noPaper;
    }

    public void setNoPaper(boolean noPaper) {
        this.noPaper = noPaper;
    }

    public  boolean isPrintHeadError() {
        return printHeadError;
    }

    public void setPrintHeadError(boolean printHeadError) {
        this.printHeadError = printHeadError;
    }

    public  boolean isPiperJamed() {
        return piperJamed;
    }

    public void setPiperJamed(boolean piperJamed) {
        this.piperJamed = piperJamed;
    }

    public  boolean isDorOpen() {
        return dorOpen;
    }

    public void setDorOpen(boolean dorOpen) {
        this.dorOpen = dorOpen;
    }

    public  boolean isDayIsOpen() {
        return dayIsOpen;
    }

    public  void setDayIsOpen(boolean dayIsOpen) {
        this.dayIsOpen = dayIsOpen;
    }

    public  boolean isReceiptIsOpen() {
        return receiptIsOpen;
    }

    public void setReceiptIsOpen(boolean receiptIsOpen) {
        this.receiptIsOpen = receiptIsOpen;
    }

    public String getHeader(){
        SimpleDateFormat sdf=new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        String stringTime=sdf.format(date);
        return String.format("ЗН ККТЖ:%s  РН ККТ:%s\nФН:%s\n%s",znKKT,rhKKT,fh,stringTime);
    }

    public String getNds() {
        return nds;
    }

    public void setNds(String nds) {
        this.nds = nds;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }


    public int getCountZeroize() {
        return countZeroize;
    }

    public void setCountZeroize(int countZeroize) {
        this.countZeroize = countZeroize;
    }

    public  boolean isConnection() {
        return isConnected;
    }

    public void setConnected(boolean connected){
        this.isConnected=connected;
    }


    public  State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }
    @Override
    public String toString() {
        return "{\"printer\":{\n\"flags\":{" +
                "\"printOn\":" +printOn+
                ",\"piperJamed\":"+piperJamed+
                ",\"lowPaper\":"+lowPaper+
                ",\"noPaper\":"+noPaper+
                ",\"printHeadError\":"+printHeadError+
                ",\"dorOpen\":"+dorOpen+
                "},\n \"state\":{" +
                "\"dayIsOpen\":"+dayIsOpen+",\"receiptIsOpen\":"+receiptIsOpen+",\"day24H\":"+day24H+"}" +
                "}\n}";
    }
}
