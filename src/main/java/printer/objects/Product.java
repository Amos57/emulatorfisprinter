package printer.objects;

public class Product {

    //количество
    private long quatity;

    //имя продукта
    private String name;

   //цена
    private long price;

    public Product(String name, long quatity, long price) {
        this.quatity = quatity;
        this.name = name;
        this.price=price;
    }

    public void setQuatity(long quatity){
        this.quatity=quatity;
    }

    public long getPrice(){return price;}

    public long getQuatity() {
        return quatity;
    }

    public String getName() {
        return name;
    }


    @Override
    public String toString() {
        return name;
    }
}

