package printer.objects;

import ru.fiscal.data.PayTipe;

import java.io.Serializable;
import java.math.BigDecimal;



public class Check implements Serializable {

    //сегодняшняя дата и время
   // private Date date;

    //cумма до сдачи
    private BigDecimal sum;


    /**
    cумма до сдачи
     */
    private BigDecimal sumOftheItem;


    private String text = "default text";


    /**
     * тип оплаты
     *
     */
    private PayTipe typePay;




    public BigDecimal getSum() {
        return sum;
    }

    public void setSum(BigDecimal sum) {
        this.sum = sum;
    }

    public BigDecimal getSumOftheItem() {
        return sumOftheItem;
    }

    public void setSumOftheItem(BigDecimal sum) {
        this.sumOftheItem = sumOftheItem;
    }

    public String getText() { return text;}

    public void setText(String text) {
        this.text = text;
    }

    public PayTipe getTypePay() {
        return typePay;
    }

    public void setTypePay(PayTipe typePay) {
        this.typePay = typePay;
    }

}